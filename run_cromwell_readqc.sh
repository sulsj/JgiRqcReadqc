#!/bin/bash
# $1: input file
# $2: library name
# $3: output dir
set -e
sed "s/<input_file>/$1/g" < /kb/module/readqc_kb.wdl.template >/kb/module/readqc_kb.wdl &&
sed "s/<lib_name>/$2/g" < /kb/module/readqc_kb.wdl >/kb/module/readqc_kb.wdl.updated &&
sed "s/<output_dir>/$3/g" < /kb/module/readqc_kb.wdl.updated >/kb/module/readqc_kb.wdl &&
rm /kb/module/readqc_kb.wdl.updated &&
java -jar /kb/module/cromwell.jar run /kb/module/readqc_kb.wdl