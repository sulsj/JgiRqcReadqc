task readqc {
    command {
        /kb/module/readqc.sh -f x -o z -r 0 -l y --skip-blast -m 0
    }
    
    output {
        String out = read_string(stdout())
    } 
}

workflow readqcwf {
    call readqc
}
